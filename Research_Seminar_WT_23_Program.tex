%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ESAGA Research Seminar on Variation of GIT quotients
% (Preliminary Program)
%
% LaTeX Sourse Code
% Version 1.0 (01/09/2023)
%
% Author:
% Niklas Maximilian Müller (Universität Duisburg-Essen)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\documentclass[a4paper, DIV=calc, 11pt,  BCOR=0.5cm, twoside]{article} 

%%%             Format                          %%%

\hoffset -1in
\oddsidemargin 3.5cm
\evensidemargin 3cm
\textwidth 15cm
\usepackage[british]{babel} %Silbentrennung
\usepackage[utf8]{inputenc}
\usepackage{microtype} 
\usepackage{csquotes}

%%%             Bibliography                    %%%

\usepackage[
backend=biber,
sorting=nyt,
style=alphabetic,
maxbibnames=10
]{biblatex}
\addbibresource{Literatur.bib}
\renewbibmacro{in:}{}
\DeclareFieldFormat
  [article,inbook,incollection,inproceedings,patent,thesis,unpublished]
  {title}{#1\isdot}


\hfuzz5pt
\clubpenalty 50000
\widowpenalty 50000

\usepackage{enumitem}
\setlist{  
	listparindent=\parindent,
	parsep=0pt,
}

\usepackage{dsfont, mathtools, amssymb, delarray, graphicx, nicefrac, import, cancel, mathrsfs, esint, framed, stmaryrd, pdfpages, amsmath}
\usepackage[pdfencoding=unicode, psdextra, colorlinks=false, pdfborder={0 0 0.7}]{hyperref}

\renewcommand{\labelenumi}{$\mathrm{(\roman{enumi})}$}
\renewcommand{\labelenumii}{$\mathrm{(\arabic{enumii})}$}
\renewcommand{\labelenumiii}{$\mathrm{(\alph{enumiii})}$}



\usepackage[capitalize,nameinlink]{cleveref}
\crefname{Section}{Section}{Sections}





\title{Research Seminar on VGIT - Program}
\author{Niklas Müller}
\date{26/10/2023}

\begin{document}

\maketitle


\subsection*{Talk 0: Overview and Distribution of Topics (12/10/23)}

On the first Thursday of the new term I will start by giving an overview of the topics that we will cover in the course. Afterwards we will distribute the talks which have not then found a speaker yet.

\section{Geometric Invariant Theory \& Toric Geometry}

In the first part of the seminar we will discuss classical results concerning \emph{Geometric Invariant Theory} (or \emph{GIT} for short) which will be necessary to understand the topics we will cover later on in the term. We start by learning about toric varieties in talk 1 which will hopefully provide a great source of examples throughout the seminar. In talk 2 we cover some preliminary results concerning line bundles. Then starting from talk 3 we turn towards GIT: We first discuss the affine case before turning to the projective case in talk 4. Finally, talk 5 and 6 cover two valuable tools we will require later: The Hilbert-Mumford Criterion which is the prefered way to check in practice whether a given point is semistable or not and the Hesselink stratification of the unstable locus.


\subsection*{Talk 1: A Crash-Course in Toric Geometry (19/10/23)}
\label{Talk A Crash-Course in Toric Geometry}

In this talk we will learn about (normal, semiprojective) toric varieties. The main goal is to be able to describe how the toric variety associated to a rational polyhedron looks like, especially in explicit examples. The material for this talk should be mostly contained in \cite[§ 2 and § 7.1]{CoxLittleSchenck}. If you consider giving this talk you should have probably heard of toric varieties before.

Start by introducing some basic terminology regarding polyhedra. Then explain how to associate (directly, without passing through the fan) a toric variety to a polyhedron. Continue by explaining how to `see' this toric variety: Namely, that the interior corresponds to the torus, the facets correspond to divisors in the boundary, vertices correspond to fixed points\dots . Make sure to give plenty examples of low dimensional polyhedra and give the audience time to understand what the corresponding variety is. You might also want to explain how to see smoothness combinatorially.

At this point it would probably be nice to mention for those with prior knowledge how to obtain a fan from a polyhedron and to make the connection with the usual picture. Finally, you should explain how morphisms between toric varieties are reflected by their polyhedra. In particular, it would be nice to see some examples of blow-ups. 






\subsection*{Talk 2: Preliminaries: The Ample Cone (26/10/23)}
\label{Talk Preliminaries: The Ample Cone}

The goal of this talk is to review some key concepts from algebraic geometry, namely divisors, line bundles and the morphisms encoded by them, and to understand how to interpret them in terms of toric geomerty. The main source is \cite[§ 4, 6, 7]{CoxLittleSchenck} but \cite[§ II.6, II.7]{hartshorne_AlgebraicGeometry} might also be helpful. If you consider giving this talk it might be helpful if you have worked with line bundles and divisors before.

Start by summarising \cite[§ 4.0]{CoxLittleSchenck}. In particular, you should introduce the concepts of \emph{Weil} and \emph{Cartier divisors} on normal varieties, rational equivalence between them and the \emph{Class} and \emph{Picard group}. State that the Picard group of a normal proper variety is an abelian variety. Explain how to read of this information for toric varieties from its polytope following \cite[§ 4.1, 4.2]{CoxLittleSchenck}.

Proceed by defining the sheaf associated to a Weil divisor and state that this defines a bijection between the class group and iso classes of reflexive invertible sheaves. Mention that Cartier divisors correspond to line bundles under this construction. Then introduce the section polyhedron of a divisor on a toric variety and explain how it relates to the global sections of the associated sheaf, see \cite[§ 4.3]{CoxLittleSchenck}. 

Define the section ring of a line bundle. Assuming it is finitely generated, define the assciated Iitaka fibration of the line bundle which is a dominant rational map. Here and throughout we use Grothendieck's definition of $\mathds{P}^n$.  Define what it means to be (semi)ample. State that the section ring of a semiample bundle is always finitely generated. Explain how to obtain these morphisms in the toric world (here, the key result is \cite[Proposition 6.2.5]{CoxLittleSchenck}).



\iffalse
\subsection{Preliminaries on Reductive Groups}

The goal of this talk is to summarise concisely the main results one should know about reductive algebraic groups and learn about Nagata's theorem. This material is well-known and classical but it is maybe somewhat difficult to summarise it efficiently, it might be helpful if you heard of reductive groups before in case you consider giving this talk. \cite{milne_algebraicGroups} is the standard source but somewhat long winded; you might find \cite{Makisumi_ReductiveGroups} helpful.

Start by defining (affine) algebraic groups as Zariski-closed subgroups of $\textmd{GL}_n$. Introduce the groups $\mathds{G}_m$ and $\mathds{G}_a$ explicitly. Define Unipotent groups and Tori and explain how to characterise them in terms of their representation theory. Then define the (unipotent) radical of an (affine) algebraic group and define reductive and semisimple groups. Give many examples illustrating the above definitions, in particular $\textmd{GL}_n, \textmd{SL}_n$ and possibly $\textmd{SO}_n$, \dots.

Recall the most important facts about reductive groups. In particular, you should introduce Borel subgroups and parabolic subgroups and you should define the Weyl-group. Explain in detail how all of this looks at least for $\textmd{GL}_n$.

\begin{itemize}
    \item Explain how representations of reductive groups are determined by the weights of the action of a maximal torus
    \item Define what it means for an affine algebraic group to act on a variety, state Nagata's theorem
    \item If time permits: Talk about linearly reductive groups,...
\end{itemize}
\fi


\subsection*{Talk 3: Classical Invariant Theory (02/11/23)}
\label{Talk Classical Invariant Theory}

In this talk we want to discuss the classical method to define quotients of affine schemes by reductive groups in algebraic geometry. Our focus is on understanding how close these quotients are to actual orbit spaces aka set-theoretic quotients. The main result is that GIT quotients are always examples of \emph{good} quotients. The material for this talk is mostly contained in \cite[§ 2.6, § 3]{Hoskins_GITandSymplecticReduction}, however we will take a somewhat different point of view.

Start by defining \emph{categorical} and \emph{geometric quotients} of schemes. Explain by means of examples why both of these notions are not satisfactory in general. Proceed by giving the modern definition of good quotients. State Nagata's theorem on actions of reductive groups as a black box and use it to deduce that quotients by reductive groups have many desirable properties, see \cite[§ 2.6]{Hoskins_GITandSymplecticReduction}.

Then, in the affine setting, define the GIT quotient. Observe that it is (essentially tautologically) a good quotient. Continue by defining (poly) stable points and stating \cite[Proposition 3.19]{Hoskins_GITandSymplecticReduction}. Conclude by discussing \cite[Examples 3.20, 3.22]{Hoskins_GITandSymplecticReduction}.




\subsection*{Talk 4: Linearisations and GIT on projective varieties (09/11/23)}
\label{Talk Linearisations and GIT on projective varieties}

The goal of this talk is to define good quotients for actions of reductive groups on projective varieties following the treatment in \cite[§ 4]{Hoskins_GITandSymplecticReduction}. To this end we need to learn about linearisations. Regarding the latter, \cite{brion_LinearisationsOfGroupActions} is a good and concise reference.

Start by recalling some facts about ample line bundles which we were not able to cover in talk 2: Recall the definition of $\textmd{Pic}(X)$ and state that if $X$ is normal and proper then $\textmd{Pic}^0$ is an abelian variety. Talk about algebraic equivalence of line bundles and introduce the \emph{Neron-Severi group} $\textmd{NS} := \textmd{Pic}/ \textmd{Pic}^0$ and $\textmd{N}^1_{\mathds{R}}$. Introduce the ample cone, which is an open strictly convex cone in $\textmd{N}^1_{\mathds{R}}$. Give some examples to show that this cone may or may not be rational.

Then define what it means for a line bundle to be invariant under the action of a group and what it means for a group action to be \emph{linearised} on an ample line bundle. Briefly sketch, why on normal varieties linearisations for actions of affine algebraic groups always exist after passing to some multiple (for invariance argue via the Picard as in \cite[Corollary 1.6]{Mumford_GIT} and for the linearisability argue via a projective embedding).

For the rest of the talk we follow \cite[§ 4]{Hoskins_GITandSymplecticReduction}: Define GIT quotients relative to a linearisation as in \cite[Definition 4.24]{Hoskins_GITandSymplecticReduction}. Introduce the notions of \emph{stable, polystable} and \emph{semistable} points. Prove in detail that the GIT quotient is a good quotient over the locus of semistable points and a geometric quotient over the locus of polystable points. Here, the most important statements are \cite[Theorem 4.26]{Hoskins_GITandSymplecticReduction} and \cite[Theorem 4.30]{Hoskins_GITandSymplecticReduction}. Definitely mention the example of the projective space.

Finally, you should explain how, and why, any (normal, semiprojective) toric variety arises as a GIT quotient. The relevant material is contained in \cite[Section 14.2]{CoxLittleSchenck}. You should start by stating \cite[Example 14.2.14]{CoxLittleSchenck} which is the result we are mainly interested in. Regarding the proof, you should at least outline \cite[Theorem 14.2.3]{CoxLittleSchenck}. Give some explicit (!) examples.

\subsection*{Talk 5: The Hilbert-Mumford Criterion (16/11/23)}

In this talk we want to prove the Hilbert-Mumford criterion which is the preferred way to check whether a point is (semi) stable in practice. For concreteness we will mostly stick with algebraic tori for this talk. The general reductive case will be briefly outlined only at the very end. The material for this talk is almost entirely contained in \cite[Section 5]{Hoskins_GITandSymplecticReduction}. 

Start by proving \cite[Proposition 5.1]{Hoskins_GITandSymplecticReduction}. Then state the numerical Hilbert-Mumford criterion \cite[Theorem 5.15]{Hoskins_GITandSymplecticReduction}. Prove it for algebraic tori following \cite[§ 5.4]{Hoskins_GITandSymplecticReduction}. Deduce that the GIT quotient only depends on the algebraic equivalence class of the linearisation.

Then introduce the weight polytope and explain how it captures stability, see \cite[Proposition 5.17]{Hoskins_GITandSymplecticReduction}. 

You should then explain how to see the weight polytopes in the GIT quotient construction of toric varieties from talk 4. As an application, compute the semistable locus in this situation, cf.\ \cite[Proposition 14.2.21]{CoxLittleSchenck}.

In the end you should briefly explain how all of this looks for arbitrary reductive groups and what are the main ingredients in upgrading from the torus case to the general case.


\subsection*{Talk 6: The Hesselink Stratification (23/11/23)}

The goal of this talk is to introduce an important stratification of the unstable locus and study its properties. This will be the key to proving finiteness results for VGIT later on. The main reference for this talk is \cite[§ 3.3]{hoskins_SurveyModuliandGIT}.

Begin by introducing the set of destabilising one-parameter subgroups $\Lambda$, see \cite[Definition 3.17]{hoskins_SurveyModuliandGIT}. 

Next, define the Hesselink–Kempf–Kirwan–Ness stratification of the unstable locus as in \cite[Theorem 3.20]{hoskins_SurveyModuliandGIT}. Explain what the strata look like \cite[Proposition 3.27]{hoskins_SurveyModuliandGIT}, if time permits you can also give some details regarding the proof. More importantly for us, however, you should explain how to prove that there can only be finitely many such strata, regardless of the linearisation used. This is \cite[Theorem 1.3.9]{DolgachevHu_VariationofGeometricInvariantTheoryQuotients}. Alternatively, \cite{Schmitt_ASImpleProofOfFinitenessofVGIT} promises to give a more elementary proof.

If time permits, as an application you could state and discuss Kirwans theorem on how to compute the singular cohomology of geometric quotient from $G$-equivariant cohomology on the original space.








\section{Variational Theory}

In the second part of the seminar we are going to discuss the basic theory there are to learn about varying the linearisation defining the GIT quotient. To this end, we will prove in talk 7 and 8 that there is a finite wall-and-chamber decomposition of the cone of linearisations such that the GIT quotient does not change when varying the linearisation in a chamber. Then we will see in talk 9 that the quotients in two adjacent chambers are related by a certain kind of birational transformation.


\subsection*{Talk 7: Stability Sets and GIT classes (30/11/23)}

The goal of this talk is to introduce a partition of the cone of ample linearisations into so-called \emph{GIT-classes}, i.e.\ subsets for which the semistable and polystable loci are independent of the chosen linearisation in the subset. 

Start by briefly recalling the results in \cite[§ 1, 2]{Ressayre_VGIT}. Most of these should have been covered in talks 5 and 6. Then discuss the results in \cite[§ 3]{Ressayre_VGIT} in detail. In the end you should also explain how to see the stability sets in the toric picture; the most relevant result here is \cite[Proposition 15.2.1]{CoxLittleSchenck}.



\subsection*{Talk 8: The GIT Fan (07/12/23)}

The goal of this talk is to show that the GIT-classes form a simple combinatorial structure, a so-called fan.

Discuss the results in \cite[§ 4, 5]{Ressayre_VGIT} in detail; the goal is to prove and understand the general wall and chamber decomposition on the set of linearisations.

Next, we turn to the toric case: Start by introducing the secondary fan using \cite[Proposition 15.2.1]{CoxLittleSchenck} as your definition. Give as many examples as possible to illuminate certain features of the secondary fan according to your liking, see \cite[§ 14.4]{CoxLittleSchenck}. You can find plenty examples to choose from in \cite[§ 14, 15]{CoxLittleSchenck}. Make sure to cover at least the Hirzebruch surface thought. You might want to mention that the secondary fan is, under some mild hypothesis, induced from a polytope.

Conclude by giving the example mentioned in \cite[p.18 ]{Ressayre_VGIT} showing that it is possible that $X^{ss} \neq X^s$ even when the GIT class is a chamber. You should also explain why this can not happen in the toric case.




\subsection*{Talk 9: Crossing a Wall (14/12/23)}

In this talk we want to understand how GIT-quotients in adjacent GIT-chambers are related to each other. This is commonly referred to as \emph{wall-crossing}. The material for this talk is mostly contained in \cite[§ 6]{Ressayre_VGIT}.

Start by explaining why specialisation of GIT-classes yields morphisms between the respective quotients. Observe that these morphisms are birational in all cases but possibly on the boundary of the cone of linearisations. Illuminate the possible behaviour by giving many examples from the toric world. In particular, you should give at least one example were the birational transform is a divisorial contraction and at least one were it is a flip. 

Continue by discussing the result in \cite[§ 6]{Ressayre_VGIT} which determines very explicitly how the exceptional loci may look like. In particular, for quotients by algebraic tori the exceptional loci must be toric.

If time permits it would also be nice to see how the Hesselink stratification changes when crossing a wall. This gives a different point of view on the same phenomenon.







\section{Application: Cohomology of the Moduli Space of Higgs Bundles on a Curve}

The goal of this series of talks is to discuss, as an application of the theory we developped in the seminar, the computation of the cohomology of the moduli space of Higgs bundles on a curve following Garcia-Prada-Heinloth-Schmidt outlined in \cite{heinloth_HauselsConjecture}.

\subsection*{Talk 10: The Moduli Space of Higgs Bundles (21/12/23)}

The goal of this talk is to introduce Higgs Bundles and understand at least parts of the GIT construction of their moduli space.

Start by defining what a \emph{Higgs bundle} is on a curve. Recall the classical notion of \emph{slope stability} of Higgs bundles. As a motivation for why one would consider these objects, you might want to mention Simpson's correspondence: There is an equivalence of categories between semistable Higgs bundles of degree zero and representations of the fundamental group. Introduce the moduli functor of Higgs Bundles and state that it is an algebraic stack. 

The GIT construction of the moduli space of stable Higgs bundles is somewhat technical and lengthy so you probably want to explain the construction in detail only for ordinary vector bundles. The case of Higgs bundles then works similar; the earlier situation is explained very nicely in \cite[§ 8]{Hoskins_ModuliandGIT}.

Start by proving \cite[Lemma 8.36]{Hoskins_ModuliandGIT}. Introduce the \emph{Quot-scheme}; its existence \cite[Lemma 8.43]{Hoskins_ModuliandGIT} should be treated as a black box. Explain why the Quot-scheme parametrises all semistable vector bundles, see \cite[Lemma 8.49]{Hoskins_ModuliandGIT}. The real work is done in  \cite[§ 8.9]{Hoskins_ModuliandGIT} where one has to show that slope semistable bundles are precisely the GIT semistable points in this picture. You will probably only be able to sketch this part. Conclude with \cite[Theorem 8.64]{Hoskins_ModuliandGIT}.

Finallly, discuss the basic properties of the Moduli space of Higgs bundles given in \cite[§ 2]{heinloth_HauselsConjecture}. In particular you should mention the Hitchin fibration and you should explain how one can use it to see that 






\subsection*{Talk 11: A Conjecture of Hausel and Rodriguez-Villegas (11/01/24)}

The content of this talk is to motivate and explain the motivic formulation of Hausel's and Rodriguez-Villegas conjecture regarding the cohomology of the moduli space of Higgs bundles. Then we derive and finally prove their conjectural formula for the $y$-genus. 

Start by introducing the Hitchin fibration and explaining how it can be used to deduce that the cohomology of the Moduli space of Higgs bundles is pure, see \cite[Theorem 1]{heinloth_HauselsConjecture}. Continue by motivating and explaining the motivic formulation of Hausel's conjecture as in \cite[§ 3]{heinloth_HauselsConjecture}. Deduce their formula \cite[Conjecture 7]{heinloth_HauselsConjecture} for the $y$-genus. To prepare for this section of the talk you might want to consoledate Jochen as an additional source.

In the rest of the talk, we want to understand the proof of Hausel's conjecture. You should explain as much of it as reasonably possible. The proof uses wall-crossing and you should focus on this part. The overview provided in \cite[§ 4]{heinloth_HauselsConjecture} should be helpful. You might also want to consider \cite[§ 8]{Thaddues_VGIT} which provides a proof for a similar but easier statement.

If you consider giving this talk you should probably feel somewhat familiar with algebraic stacks.






\section{Further Topics (18/01/24 \& 25/01/24)}

The goal of these final two talks would be to get a glimpse at some further developments in recent years.

%\subsection*{Hasset-Keel Program}

%The \emph{Hasset-Keel program} originally aimed to understand all compactifications of $\textmd{M}_g$, the moduli space of smooth curves of genus $g$, occuring when running a log minimal model program starting with the space of semistable curves. In particular, it aims to understand what sorts of curves these spaces classify. In fact, the birational transformations mentioned above can be obtained for example using VGIT and so we recover the connection with the seminar. 

%In practice, understanding these spaces seems like it can get rather technical. Also there are only partial results available so far, see \cite{Fedorchuk_AlternativeCompactificationsModuliStableCurves} for a rather extensive survey. However, in the case genus $g=3$ the article \cite{Hyeon_MMPModuliStableCurvesGenus3} seems reasonable and rather explicit. Also \cite{Codogni_HassetKeelProgram} might be worth having a look at.

\subsection*{Bridgeland Stability Conditions}

When studying moduli problems, to obtain a reasonably behaved moduli space one usually has to restrict attention to so called `stable' objects. This terminology stems from the following fact: In classical moduli problems there usually exists a parameter space $U$ for all the objects one is interested in with a group of symmetries $G$ such that the moduli space is just the GIT quotient $M := U/ \! \!/G$ and in this case the stable objects correspond precisely to the stable points for this action (with respect to some natural linearisation).

It is natural to wonder if there are other notions of stability which are not induced by some linearisation on some parameter space. In this context, Bridgeland introduced a more general axiomatic approach to defining `stable' objects in some category. These `Bridgeland-stability' notions usually are not induced by some linearisation on some parametrisation. However, one may still ask for wall-crossing like behaviour on moduli spaces when varying the stability condition. And indeed, many results in this direction are known today.

\cite{Macri_LecturesBridgelandStability} seems to give a good overview of what has been proved. One possibility would be to use one talk to set up the basic theory and then use the second talk to sketch Bayer’s proof of the Brill–Noether Theorem or Macrì-Schmidt's proof of a theorem by Gruson–Peskine and Harris on the genus of space curve. Both are conatined in the survey \cite{Macri_StabilityAndApplications}.

%\subsection*{Semi-Orthoginal Decompositions}

%In the seminar we have seen that wall-crossing induces birational transformations between the corresponding GIT-quotients. Unfortunately, in general these transformations are quite difficult to understand. It has been observed some time ago that the situation becomes somewhat nicer on the level of bounded derived categories; indeed, at least in nice situations the bounded derived categories of two GIT quotients are related by a so-called \emph{semi-orthogonal decomposition}. This observation has raised some hopes that when studying varieties up to birational equivalence it might be better to study their bounded derived categories then the varieties themselves. Also, apparently, this topic is very much connected to physics.

%Acclaimedly, in the toric situation the above should be well-understood. I think it would be nice to see some aspects of this story. One possibility would be to study some parts of the paper \cite{BFK_VGITandDerivedCategories}; acclaimedly the first ArXiv version contains a very nice account of what happens for actions by $\mathds{G}_m$.

%\subsection*{Computational Aspects}

%The recent preprint \cite{gallardo_ComputationalAspectsofGIT} presents a new algorithm, implemented in \texttt{SageMath}, to compute explicitly the (semi)stable locus of an action by a semisimple algebraic group. This algorithm is then used to examine some classical moduli spaces of cubic surfaces and a moduli space of $K$-stable Fanos. If someone is interested in this material this might also make for a nice talk.


% ------------------------------------------------------------------------------------ %											
%                                   Bibliography									   %
% ------------------------------------------------------------------------------------ %
\printbibliography

\end{document}
